SRC=$(shell pwd)
LLVM_SRC=$(SRC)/llvm-6.0.0.src
CLANG_SRC=$(SRC)/cfe-6.0.0.src
LLD_SRC=$(SRC)/lld-6.0.0.src
LIBCXX_SRC=$(SRC)/libcxx-6.0.0.src
COMPILER_RT_SRC=$(SRC)/compiler-rt-6.0.0.src
LIBXML2_SRC=$(SRC)/libxml2-2.9.7
ZLIB_SRC=$(SRC)/zlib-1.2.11
MUSL_SRC=$(SRC)/musl-1.1.19
ZIG_SRC=$(SRC)/zig-0.2.0
NATIVE_PREFIX=$(SRC)/out/native
TARGET_PREFIX=$(SRC)/out/target
STAGE1_PREFIX=$(SRC)/out/stage1

.PHONY: all
all: $(NATIVE_PREFIX)/bin/zig

$(STAGE1_PREFIX)/bin/zig: $(NATIVE_PREFIX)/bin/lld $(NATIVE_PREFIX)/bin/clang $(NATIVE_PREFIX)/bin/llvm-config
	mkdir -p out/build-stage1-zig
	cd out/build-stage1-zig; \
	cmake $(ZIG_SRC) -DCMAKE_PREFIX_PATH=$(NATIVE_PREFIX) -DCMAKE_INSTALL_PREFIX=$(STAGE1_PREFIX) -DCMAKE_BUILD_TYPE=Release -DZIG_LIBC_LIB_DIR=$(NATIVE_PREFIX)/lib -DZIG_LIBC_INCLUDE_DIR=$(NATIVE_PREFIX)/include -DZIG_LIBC_STATIC_LIB_DIR=$(NATIVE_PREFIX)/lib; \
	$(MAKE) install

$(NATIVE_PREFIX)/bin/zig: $(STAGE1_PREFIX)/bin/zig
	mkdir -p out/build-native-zig
	cd out/build-native-zig; \
	$(STAGE1_PREFIX)/bin/zig build --build-file $(ZIG_SRC)/build.zig --prefix $(NATIVE_PREFIX) --search-prefix $(NATIVE_PREFIX) install
	$(MAKE) install

$(NATIVE_PREFIX)/bin/llvm-config: $(LLVM_SRC)/CMakeLists.txt $(NATIVE_PREFIX)/lib/libz.a $(NATIVE_PREFIX)/lib/libxml2.a
	mkdir -p out/build-native-llvm
	cd out/build-native-llvm; \
	cmake $(LLVM_SRC) -DCMAKE_PREFIX_PATH=$(NATIVE_PREFIX) -DCMAKE_INSTALL_PREFIX=$(NATIVE_PREFIX) -DCMAKE_BUILD_TYPE=Release -DLLVM_EXPERIMENTAL_TARGETS_TO_BUILD=WebAssembly; \
	$(MAKE) install

$(NATIVE_PREFIX)/lib/libz.a: $(ZLIB_SRC)/configure
	mkdir -p out
	cp -r $(ZLIB_SRC) out/build-native-zlib
	cd out/build-native-zlib; \
	./configure --static --prefix=$(NATIVE_PREFIX); \
	$(MAKE) install

$(NATIVE_PREFIX)/lib/libxml2.a: $(LIBXML2_SRC)/configure
	mkdir -p out
	cp -r $(LIBXML2_SRC) out/build-native-libxml2
	cd out/build-native-libxml2; \
	./configure --without-python --disable-shared --prefix=$(NATIVE_PREFIX); \
	$(MAKE) install

$(NATIVE_PREFIX)/bin/clang: $(CLANG_SRC)/CMakeLists.txt $(NATIVE_PREFIX)/bin/llvm-config
	mkdir -p out/build-native-clang
	cd out/build-native-clang; \
	cmake $(CLANG_SRC) -DCMAKE_PREFIX_PATH=$(NATIVE_PREFIX) -DCMAKE_INSTALL_PREFIX=$(NATIVE_PREFIX) -DCMAKE_BUILD_TYPE=Release; \
	$(MAKE) install

$(NATIVE_PREFIX)/bin/lld: $(LLD_SRC)/CMakeLists.txt $(NATIVE_PREFIX)/bin/llvm-config
	mkdir -p out/build-native-lld
	cd out/build-native-lld; \
	cmake $(LLD_SRC) -DCMAKE_PREFIX_PATH=$(NATIVE_PREFIX) -DCMAKE_INSTALL_PREFIX=$(NATIVE_PREFIX) -DCMAKE_BUILD_TYPE=Release; \
	$(MAKE) install

$(TARGET_PREFIX)/lib/libc.a: $(MUSL_SRC)/configure
	mkdir -p out
	cp -r $(MUSL_SRC) out/build-target-musl
	cd out/build-target-musl; \
	./configure --prefix=$(TARGET_PREFIX) --disable-shared; \
	$(MAKE) install

$(NATIVE_PREFIX)/lib/linux/libclang_rt.builtins-x86_64.a: $(COMPILER_RT_SRC)/CMakeLists.txt $(NATIVE_PREFIX)/bin/llvm-config
	mkdir -p out/build-target-compiler-rt
	cd out/build-target-compiler-rt; \
	cmake $(COMPILER_RT_SRC) -DCMAKE_PREFIX_PATH=$(NATIVE_PREFIX) -DCMAKE_INSTALL_PREFIX=$(NATIVE_PREFIX) -DCMAKE_BUILD_TYPE=Release -DCOMPILER_RT_BUILD_SANITIZERS=off -DCOMPILER_RT_BUILD_XRAY=off -DCOMPILER_RT_BUILD_LIBFUZZER=off; \
	$(MAKE) install

$(NATIVE_PREFIX)/lib/clang/6.0.0/lib/linux/libclang_rt.builtins-x86_64.a: $(NATIVE_PREFIX)/lib/linux/libclang_rt.builtins-x86_64.a
	mkdir -p $(NATIVE_PREFIX)/lib/clang/6.0.0/lib/linux
	cp $(NATIVE_PREFIX)/lib/linux/libclang_rt.builtins-x86_64.a $(NATIVE_PREFIX)/lib/clang/6.0.0/lib/linux/libclang_rt.builtins-x86_64.a

# TODO this is not linking against musl correctly
$(TARGET_PREFIX)/lib/libc++.a: $(LIBCXX_SRC)/CMakeLists.txt $(NATIVE_PREFIX)/bin/clang $(NATIVE_PREFIX)/bin/lld $(NATIVE_PREFIX)/lib/clang/6.0.0/lib/linux/libclang_rt.builtins-x86_64.a
	mkdir -p out/build-target-libcxx
	cd out/build-target-libcxx; \
	cmake $(LIBCXX_SRC) -DCMAKE_PREFIX_PATH=$(TARGET_PREFIX) -DCMAKE_INSTALL_PREFIX=$(TARGET_PREFIX) -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=$(NATIVE_PREFIX)/bin/clang -DCMAKE_CXX_COMPILER=$(NATIVE_PREFIX)/bin/clang++ -DCMAKE_SYSTEM_NAME=Linux -DCMAKE_FIND_ROOT_PATH=$(TARGET_PREFIX) "-DCMAKE_EXE_LINKER_FLAGS=-fuse-ld=lld -rtlib=compiler-rt -Wl,-L,$(TARGET_PREFIX)/lib" "-DLINK_LIBRARIES=aoeu.o"; \
	$(MAKE) install
