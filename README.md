# Single Tarball Bootstrap of Zig

See https://github.com/zig-lang/zig/issues/853

## Requirements

 * C++ compiler
 * cmake
 * automake
 * autoconf
 * pkgconfig
